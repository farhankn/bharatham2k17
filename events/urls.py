from django.conf.urls import url, include
from rest_framework import routers
#import api
from .views import *
from eventregister.views import *

urlpatterns = (
    url(r'^proshow/$',proshow),
    url(r'^$',event_list),
    url(r'^(?P<slug>\S+)/registerteam/$', team_reg),
    url(r'^(?P<slug>\S+)/register/$', event_reg),
    url(r'^(?P<slug>\S+)/$',event_details,name='detail'),


)
