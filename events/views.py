from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

from django.template import loader

#from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import *#event
from .forms import eventForm
from eventregister.models import *


def event_list(request):
	object_list = event.objects.all()


	context = {
		#"object":object
		"object_list":object_list
	}
	return render(request,"eventdetails_list.html",context)

def event_details(request,slug=None):
	current_user=request.user

	eobject = get_object_or_404(event, slug=slug)
	ehead = eventhead.objects.filter(eventname=eobject)
        e_regs = event_registration.objects.all()
        t_regs = team.objects.all()
        flag = 0
        flag2 = 0
        if eobject.eventtype == "group":
    			flag2 = 1
	try:
		if e_regs.filter(user=current_user,event=eobject).exists():
			flag = 1

	except:
		pass


	context = {
		"object":eobject,
		"flag":flag,
		"flag2":flag2,
		"ehead":ehead,
		"slug":slug,

	}
	return render(request,"eventdetails_detail.html",context)


def mobile(request):
    template = loader.get_template('mobi/index.html')
    context = {}
    return HttpResponse(template.render(context, request))

def mobilecontact(request):
    template = loader.get_template('mobi/contact.html')
    context = {}
    return HttpResponse(template.render(context, request))


def proshow(request):
    template = loader.get_template('proshow.html')
    context = {}
    return HttpResponse(template.render(context, request))
