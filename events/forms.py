from django import forms
from .models import event


class eventForm(forms.ModelForm):
    class Meta:
        model = event
        fields = ['name', 'description', 'image', 'shortinfo', 'venue', 'eventtype', 'num_participants', 'date']

