# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-05 14:54
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('slug', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from=b'name')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('description', models.TextField(max_length=1200)),
                ('image', models.URLField()),
                ('eventhead', models.CharField(max_length=60)),
                ('contactnumber', models.CharField(max_length=60)),
                ('shortinfo', models.CharField(max_length=30)),
                ('venue', models.CharField(max_length=30)),
                ('eventtype', models.CharField(max_length=30)),
                ('num_participants', models.IntegerField()),
                ('date', models.CharField(max_length=30)),
                ('prize', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
    ]
