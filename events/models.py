from django.core.urlresolvers import reverse
from django_extensions.db.fields import AutoSlugField
from django.db.models import *
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from django_extensions.db import fields as extension_fields


class event(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    description = models.TextField(max_length=2500)
    rules = models.TextField(max_length=2500,default =" ")
    image = models.URLField()
    fullimage = models.URLField()
    #eventhead = models.CharField(max_length=60)
    #contactnumber = models.CharField(max_length=60)
    shortinfo = models.TextField(max_length=500)
    venue = models.CharField(max_length=30)
    eventtype = models.CharField(max_length=30)
    num_participants = models.IntegerField(default=0)
    date = models.CharField(max_length=30)
    prize = models.CharField(max_length=30)#max_digits=10,decimal_places=2)
    regfee = models.CharField(max_length=30)
    priority = models.PositiveSmallIntegerField(default=0)
    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse("events:detail", kwargs={"slug": self.slug})
    
    def get_tools_url(self):
        return reverse("tools", kwargs={"slug": self.slug})
    
    def event_slug(self):
       		return self.slug
    def __str__(self):
        	return self.name
    def get_update_url(self):
        return reverse('event_event_update', args=(self.slug,))
        
        
class eventhead(models.Model):
    name = models.CharField(max_length=60)
    phone = models.CharField(max_length=12)
    eventname = models.ForeignKey(event,on_delete=models.CASCADE)
