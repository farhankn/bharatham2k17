from django.contrib import admin
from django import forms
from .models import *
from django.db import models
from eventregister.models import *

class eventAdminForm(forms.ModelForm):

    class Meta:
        model = event
        fields = '__all__'


class eventModelAdmin(admin.ModelAdmin):
    form = eventAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated','description', 'image', 'shortinfo', 'venue', 'eventtype', 'num_participants', 'date']
    readonly_fields = ['slug', 'created', 'last_updated']
    list_editable = ['name','description', 'image', 'shortinfo', 'venue', 'eventtype', 'num_participants', 'date']
    
    class Meta:
        model = event
        fields = '__all__'

admin.site.register(event, eventModelAdmin)




class eventheadModelAdmin(admin.ModelAdmin):
    form = eventAdminForm
    list_display = ['name','phone']
    #list_display = ['name', 'slug', 'created', 'last_updated', 'contactnumber','description', 'image', 'eventhead', 'shortinfo', 'venue', 'eventtype', 'num_participants', 'date']
    #readonly_fields = ['slug', 'created', 'last_updated']
    list_editable = ['name','phone']
    
    class Meta:
        model = eventhead
        fields = '__all__'

admin.site.register(eventhead, eventheadModelAdmin)		


