"""bharatham2k17 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url,patterns,include
from django.contrib import admin
from django.views.generic import TemplateView
from eventregister.views import *
from . import views
from events.views import *


urlpatterns = patterns('',
url(r'^admin/', include(admin.site.urls)),
url(r'^events/', include('events.urls', namespace='events')),
url(r'^$', views.index, name='index'),
url(r'^beta/', views.index1, name='index1'),
url(r'^myprofile/', event_manage),
url(r'^accounts/', include('eventregister.urls')),
url(r'^tools/(?P<slug>\S+)/download/',event_tool_dl),
url(r'^tools/(?P<slug>\S+)/',event_tool_page,name='tools'),
url(r'^tools/',event_tools),
url(r'^$',event_list),
url(r'^mobile/',mobile),
url(r'^contact/',mobilecontact),

)
