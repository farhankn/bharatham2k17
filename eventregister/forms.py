from django import forms
from registration.forms import RegistrationForm
from django.forms import ModelForm
from models import user_profile,event_registration

class UserRegForm(RegistrationForm):
    first_name = forms.CharField() 
    last_name = forms.CharField() 
    college=forms.CharField(max_length=200)
    
    phone_number = forms.RegexField(regex=r'^\+?1?\d{10,12}$', 
                                error_message = ("Phone number must be entered in the format: '+999999999'."))
    '''
    def __init__(self, *args, **kwargs):
        super(UserRegForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['username', 'first_name', 'last_name','email','password','password2','college','phone_number']
    '''
class event_registrationForm(forms.ModelForm):
    class Meta:
        model = event_registration
        fields = ['user','event']
