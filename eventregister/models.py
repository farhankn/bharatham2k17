from __future__ import unicode_literals
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User
from events.models import event

# Create your models here.


class user_profile(models.Model):
     user=models.ForeignKey(User, unique=True)
     college=models.CharField(max_length=200)
     phone_regex = RegexValidator(regex=r'^\+?1?\d{10,12}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
     phone_number = models.CharField(validators=[phone_regex],max_length=10, blank=True)
     firstname = models.CharField(max_length=255)
     lastname = models.CharField(max_length=255)
     privilege = models.PositiveSmallIntegerField(default=0)
     
     def __unicode__(self):
         return u'%s %s' % (self.user, self.college)


class event_registration(models.Model):
     user=models.ForeignKey(User,on_delete=models.CASCADE)
     event=models.ForeignKey(event,on_delete=models.CASCADE)
     
     
     
class team(models.Model):
     teamlead = models.ForeignKey(User,on_delete=models.CASCADE)
     name = models.CharField(max_length=100)
     event = models.ForeignKey(event,on_delete=models.CASCADE)
     #members = models.TextField()
