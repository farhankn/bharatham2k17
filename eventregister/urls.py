from django.conf.urls import url,patterns,include
from django.contrib import admin
from django.views.generic import TemplateView
#solution
from registration.backends.simple.views import RegistrationView
from forms import UserRegForm
from .views import *
#for adding inst & phone details of users
import regbackend


urlpatterns = patterns('',


url(r'^register/$', RegistrationView.as_view(form_class=UserRegForm)),
url(r'^', include('registration.backends.simple.urls')),


)
