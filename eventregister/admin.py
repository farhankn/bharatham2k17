from django.contrib import admin
from django.db import models as models
# Register your models here.
from .models import *
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from models import user_profile
admin.site.unregister(User)
class UserProfileInline(admin.StackedInline):
    model = user_profile
    
   
class UserProfileAdmin(UserAdmin):
    inlines = [ UserProfileInline, ]

admin.site.register(User, UserProfileAdmin)

class eventregModelAdmin(admin.ModelAdmin):
    list_display = ['user','event']

    class Meta:
        model = event_registration


admin.site.register(event_registration, eventregModelAdmin)


class teamModelAdmin(admin.ModelAdmin):
    list_display = ['teamlead','name','event']

    class Meta:
        model = team


admin.site.register(team, teamModelAdmin)
