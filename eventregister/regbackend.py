from models import user_profile
from forms import *

def user_created(sender, user, request, **kwargs):
    form = UserRegForm(request.POST)
    data = user_profile(user=user)
    data.college = form.data["college"]
    data.phone_number = form.data["phone_number"]
    data.first_name = form.data["first_name"]
    data.last_name = form.data["last_name"]
    data.firstname = form.data["first_name"]
    data.lastname = form.data["last_name"]
    data.save()

from registration.signals import user_registered
user_registered.connect(user_created)
