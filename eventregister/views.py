from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from .models import *
from events.views import *
# Create your views here.
from django.views.generic import DetailView

import csv

'''
from django import template
register = template.Library()


@register.simple_tag
def get_obj(user):#(pk, attr):
    obj = getattr(user_profile.objects.get(user=user))
    return obj

'''


@login_required(login_url='/accounts/login/')
def event_reg(request, slug=None):


		current_user = request.user
		current_slug=slug
		current_event = get_object_or_404(event, slug=slug)
		ereg=event_registration(user=current_user,event=current_event)
		ereg.save()

		current_event.num_participants += 1
		current_event.save()
		return redirect('/events/'+slug+'/')


@login_required(login_url='/accounts/login/')
def team_reg(request, slug=None):


	if request.method == "POST":	
		current_user = request.user
		current_slug=slug


		current_event = get_object_or_404(event, slug=slug)
		teamname = request.POST.get("tname","")
		newteam = team(teamlead = current_user, name = teamname, event = current_event)
		newteam.save()
		
		ereg=event_registration(user=current_user,event=current_event)
		ereg.save()
		
		return redirect('/events/'+slug+'/')





@login_required(login_url='/accounts/login/')
def event_manage(request):
	current_user = request.user

	user_det = get_object_or_404(user_profile,user=current_user)
	eregs = event_registration.objects.all()
	exists = 0
	current_profile = None
	for obj in eregs:
		if obj.user == current_user:
			exists = 1
			break
	if exists == 1:
		current_profile = event_registration.objects.filter(user=current_user)
	context = {
			"u":current_user,
			"v":user_det,
			"current_profile":current_profile,
			"flag": exists,
			

	}
		
	return render(request, "reg_events.html", context)
	
	'''
	else:
		return render(request, "noevents.html")
	'''
@login_required(login_url='/accounts/login/')	
def event_tools(request):
	current_user = request.user
	flag = 0
	current_profile = None
	try:
		current_profile = get_object_or_404(user_profile, user = current_user)
	except:
		pass

	if request.user.is_superuser or current_profile.privilege == 1:

		event_list = event.objects.all()
		#profile_list = user_profile.objects.all()
		#ereg_list = event_registration.objects.all()
	
	
		context = {
			"event_list":event_list,
		#	"profile_list":profile_list,
		#	"ereg_list":ereg_list,
		}
		
		return render(request,"event_tools.html",context)
		
	else:
		return redirect('/events/')
			

@login_required(login_url='/accounts/login/')
def event_tool_page(request,slug=None):
	current_user = request.user
	flag = 0
	current_profile = None
	try:
		current_profile = get_object_or_404(user_profile, user = current_user)
	except:
		pass

	if request.user.is_superuser or current_profile.privilege == 1:
		#current_event = events
		flag = 0
		current_event = event.objects.get(slug=slug)
		#profile_list = user_profile.objects.all()
		ereg_list = event_registration.objects.filter(event=current_event)
		user_det=None
		if current_event.eventtype == "group":
    			flag = 1
		#user_det = user_profile.objects.all()
		reglist = []
		for obj in ereg_list:
			try:
				user_det =user_profile.objects.get(user=obj.user)
			except:
				pass
			reglist.append([user_det.firstname,user_det.lastname,user_det.college,obj.user.email,user_det.phone_number])


		context = {
			"eobj":current_event,
			#"profile_list":profile_list,
			#"eregs":ereg_list,
			"flag":flag,
			"eregs": reglist,
		}
	
		return render(request,"event_tool_page.html",context)
		
	else:
		return redirect('/events/')
			
		
@login_required(login_url='/accounts/login/')			
def event_tool_dl(request,slug=None):
	current_user = request.user
	flag = 0
	current_profile = None
	try:
		current_profile = get_object_or_404(user_profile, user = current_user)
	except:
		pass
	
	if request.user.is_superuser or current_profile.privilege == 1:
		#current_event = events
		current_event = event.objects.get(slug=slug)
		#profile_list = user_profile.objects.all()
		ereg_list = event_registration.objects.filter(event=current_event)
		
		reglist = []
		for obj in ereg_list:
			user_det =user_profile.objects.get(user=obj.user)
			reglist.append([user_det.firstname,user_det.lastname,user_det.college,obj.user.email,user_det.phone_number])
		
		
		
		
		context = {
			#"eobj":current_event,
			#"profile_list":profile_list,
			#"eregs":ereg_list,
		}
		
		response = HttpResponse(content_type='text/csv')
   		response['Content-Disposition'] = 'attachment; filename="ParticipantList.csv"'
		
  		writer = csv.writer(response)
    		writer.writerow(['FIRSTNAME', 'LASTNAME', 'COLLEGE', 'EMAIL', 'PHONE'])
    		for i in reglist:
    			writer.writerow([i[0], i[1], i[2],i[3],i[4]])
    			

    		return response
		
	else:
		return redirect('/events/')


